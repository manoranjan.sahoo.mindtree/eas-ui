import React from "react";
import ReactDOM from "react-dom";
import Header from "./components/header_and_footer/Header";
import Footer from "./components/header_and_footer/Footer";
// import SimpleSearchApp from "./components/app/SimpleSearchApp";
import AutosuggestSearchApp from "./components/app/AutosuggestSearchApp";
import "./index.css";

ReactDOM.render(
  <React.StrictMode>
    <Header />
    <div id="easMainContent" className="eas-main-content">
      {/* <div id="easCompSimpleSearchApp"></div> */}
      <div id="easCompAutosuggestSearchApp"></div>
      <div id="easCompAdditionalSearch"></div>
      <div id="easCompJobDetail"></div>
    </div>
    <Footer />
  </React.StrictMode>,
  document.getElementById("root")
);

// ReactDOM.render(<SimpleSearchApp />, document.getElementById("easCompSimpleSearchApp"));
ReactDOM.render(<AutosuggestSearchApp />, document.getElementById("easCompAutosuggestSearchApp"));
