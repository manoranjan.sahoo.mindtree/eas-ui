import React from "react";
import "./Header.css";

function Header() {
  return <header className="eas-app-header">EAS Application</header>;
}
export default Header;
