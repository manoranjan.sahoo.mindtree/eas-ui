import React, { Component } from "react";
import ReactDOM from "react-dom";
import JobDetail from "../jobDetail/JobDetail";
import "./JobsList.css";

class JobsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jobsData: props.jobsData,
      searchJobTitle: props.searchJobTitle,
      selectedLocations: props.selectedLocations
    };
  }

  showDetail = jobcode => {
    const jobDetail = this.state.jobsData.filter(j => jobcode === j.jobcode)[0];
    // console.log(jobDetail);
    ReactDOM.unmountComponentAtNode(
      document.getElementById("easCompAutosuggestSearchApp")
    );
    ReactDOM.unmountComponentAtNode(
      document.getElementById("easCompAdditionalSearch")
    );
    ReactDOM.render(
      <JobDetail jobDetail={jobDetail} />,
      document.getElementById("easCompJobDetail")
    );
  };

  render() {
    console.log('In Render():');
    console.log(this.state.searchJobTitle);
    console.log(this.state.selectedLocations);

    this.jobsDetail = this.state.jobsData
      .filter(
        j =>
          (j.title === undefined ||
            j.title.length === 0 ||
            j.title
              .toLowerCase()
              .includes(this.state.searchJobTitle.toLowerCase())) &&
          (this.state.selectedLocations.size === 0 ||
            this.state.selectedLocations.has(j.location.toLowerCase()))
      )
      .map((jobData, key) => (
        <tr key={key}>
          <td>{key + 1}</td>
          <td>{jobData.jobcode}</td>
          <td>{jobData.title}</td>
          <td>{jobData.primary_skill}</td>
          <td>{jobData.location}</td>
          <td>
            <button onClick={() => this.showDetail(jobData.jobcode)}>
              Show Detail
            </button>
          </td>
        </tr>
      ));
    return (
      <table className="eas-app-table">
        <thead>
          <tr>
            <th>#</th>
            <th>Jobcode</th>
            <th>Job Title</th>
            <th>Primary Skill</th>
            <th>Location</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{this.jobsDetail}</tbody>
      </table>
    );
  }
}

export default JobsList;
