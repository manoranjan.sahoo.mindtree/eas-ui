import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./SimpleSearchApp.css";
import mockData from "../../assets/mock.json";
import JobDetail from "../jobDetail/JobDetail";

class SimpleSearchApp extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state.jobsData = mockData;
    this.state.searchJobTitle = "";
    this.state.selectedLocations = new Set();

    let uniqueSet = new Set();
    this.state.jobsData.map((jobData, key) => uniqueSet.add(jobData.location));

    const uniqueJobLocations = [...uniqueSet];
    this.state.jobLocations = uniqueJobLocations.map((jobLocation, key) => (
      <li key={key}>
        <input
          type="checkbox"
          id={"jobLocation_" + key}
          name="selectedJobLocation"
          value={jobLocation}
          onClick={e => this.updateJobLocationFilter(e)}
        />
        <label htmlFor={"jobLocation_" + key}>{jobLocation}</label>
      </li>
    ));
  }

  handleInput = event => {
    this.setState({ searchJobTitle: event.target.value });
  };

  updateJobLocationFilter = event => {
    let tmpSelectedLocations = this.state.selectedLocations;
    if (event.target.checked) {
      tmpSelectedLocations.add(event.target.value.toLowerCase());
    } else {
      tmpSelectedLocations.delete(event.target.value.toLowerCase());
    }
    this.setState({ selectedLocations: tmpSelectedLocations });
    // console.log(this.state.selectedLocations);
  };

  showDetail = jobcode => {
    const jobDetail = this.state.jobsData
    .filter(
      j => jobcode === j.jobcode
    )[0];
    // console.log(jobDetail);
    ReactDOM.unmountComponentAtNode(document.getElementById("easCompSimpleSearchApp"));
    ReactDOM.render(<JobDetail jobDetail={jobDetail} />, document.getElementById('easCompJobDetail'));
  };

  render() {
    this.jobsDetail = this.state.jobsData
      .filter(
        j =>
          (j.title === undefined ||
            j.title.length === 0 ||
            j.title
              .toLowerCase()
              .includes(this.state.searchJobTitle.toLowerCase())) &&
          (this.state.selectedLocations.size === 0 ||
            this.state.selectedLocations.has(j.location.toLowerCase()))
      )
      .map((jobData, key) => (
        <tr key={key}>
          <td>{key + 1}</td>
          <td>{jobData.jobcode}</td>
          <td>{jobData.title}</td>
          <td>{jobData.primary_skill}</td>
          <td>{jobData.location}</td>
          <td>
            <button onClick={() => this.showDetail(jobData.jobcode)}>
              Show Detail
            </button>
          </td>
        </tr>
      ));
    return (
      <div>
        <div className="eas-app-search-fields">
        <input onChange={this.handleInput} placeholder="Enter Job Title" />

        <ul className="eas-app-sel-locations">{this.state.jobLocations}</ul>
        </div>

        <table className="eas-app-table">
          <thead>
            <tr>
              <th>#</th>
              <th>Jobcode</th>
              <th>Job Title</th>
              <th>Primary Skill</th>
              <th>Location</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.jobsDetail}</tbody>
        </table>
      </div>
    );
  }
}

export default SimpleSearchApp;
