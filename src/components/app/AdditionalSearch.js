import React, { Component } from "react";
import ReactDOM from "react-dom";
import JobsList from "../jobsList/JobsList";
import "./AdditionalSearch.css";

class AdditionalSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jobsData: props.jobDetails,
      titleSearchVal: props.titleSearchVal,
      locSearchVal: props.locSearchVal,
      uniqueJobLocations: props.uniqueJobLocations,
      additionalLocations: '',
      includeAnyLocation: false
    };
  }

  updateAnyLocation = event => {
    if (event.target.checked) {
      this.setState({includeAnyLocation: true});
    } else {
      this.setState({includeAnyLocation: false});
    }
  };

  handleInput = event => {
    console.log(event.target.value);
    this.setState({additionalLocations: event.target.value});
  };

  searchJob = () => {
    let conLocations = new Set();
    console.log('this.state.locSearchVal:');
    console.log(this.state.locSearchVal);
    console.log('this.state.additionalLocations:');
    console.log(this.state.additionalLocations);
    if(this.state.includeAnyLocation === false){
      conLocations.add(null);
      conLocations.add('');
      conLocations.add(this.state.locSearchVal.toLowerCase());
      this.state.additionalLocations.split(',').map((location, key) => conLocations.add(location.toLowerCase()));
      conLocations.delete('');
      conLocations.delete(null);
    }
    console.log(conLocations);
    ReactDOM.render(
      <JobsList
        jobsData={this.state.jobsData}
        searchJobTitle={this.state.titleSearchVal}
        selectedLocations={conLocations}
      />,
      document.getElementById("jobsListDisplay")
    );
  };

  render() {
    return (
      <div>
        <div>
          <div className="eas-app-block-micro">
            <input
              type="checkbox"
              id="willingToRelocate"
              name="willingToRelocate"
              value="willingToRelocate"
              disabled
            />
            <label htmlFor="willingToRelocate">Willing to relocate ?</label>
          </div>
          <div className="eas-app-block-micro">
            <input
              type="checkbox"
              id="withinState"
              name="withinState"
              value="withinState"
              disabled
            />
            <label htmlFor="withinState">Within State</label>
          </div>
          <div className="eas-app-block-micro">
            <input
              type="checkbox"
              id="anyLocation"
              name="anyLocation"
              value="anyLocation"
              onClick={e => this.updateAnyLocation(e)}
            />
            <label htmlFor="anyLocation">Any Location</label>
          </div>
          <div className="eas-app-block-micro">
            <label htmlFor="otherLocations">
              Enter other locations to include{" "}
            </label>
            <input
              id="otherLocations"
              name="otherLocations"
              placeholder=" seprated by comma(,)"
              onChange={this.handleInput}
            />
          </div>
        </div>
        <div className="ess-app-centre-align">
          <button onClick={() => this.searchJob()}>Search</button>
        </div>
        <div id="jobsListDisplay"></div>
      </div>
    );
  }
}

export default AdditionalSearch;
