import React from "react";
import ReactDOM from "react-dom";
import Autosuggest from "react-autosuggest";
import AdditionalSearch from "./AdditionalSearch";
import "../../assets/Autosuggest.css";
import "./AutosuggestSearchApp.css";
import mockData from "../../assets/mock.json";

let uniqueJobLocations = [];

function getSuggestionsForTitle(value) {
  const escapedValue = value.trim();

  if (escapedValue === "") {
    return [];
  }

  return mockData.filter(jobDetail =>
    jobDetail.title.toLowerCase().includes(escapedValue.toLowerCase())
  );
}

function getSuggestionsForLoc(value) {
  const escapedValue = value.trim();

  if (escapedValue === "") {
    return [];
  }

  return uniqueJobLocations.filter(jobLocation =>
    jobLocation.location.toLowerCase().includes(escapedValue.toLowerCase())
  );
}

function getSuggestionValueForTitle(suggestion) {
  return suggestion.title;
}

function getSuggestionValueForLoc(suggestion) {
  return suggestion.location;
}

function renderSuggestionForTitle(suggestion) {
  return <span>{suggestion.title}</span>;
}

function renderSuggestionForLoc(suggestion) {
  return <span>{suggestion.location}</span>;
}

class AutosuggestSearchApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      titleSearchVal: "",
      suggestionsForTitle: [],
      locSearchVal: "",
      suggestionsForLoc: [],
      searchOrResetButtonMode: 0 // 'searchOrResetButtonMode' is 0 for "Search" and 1 for "Reset"
    };

    // Loading unique locations to avoid displaying duplicate search results
    uniqueJobLocations = [];
    let uniqueSet = new Set();
    mockData.map((jobData, key) => uniqueSet.add(jobData.location));
    uniqueSet.forEach(this.convertSetToArray);
  }

  convertSetToArray = (val1, val2, setItself) => {
    uniqueJobLocations.push({ location: val1 });
  };

  onChangeForTitle = (event, { newValue, method }) => {
    this.setState({
      titleSearchVal: newValue
    });
  };

  onChangeForLoc = (event, { newValue, method }) => {
    this.setState({
      locSearchVal: newValue
    });
  };

  onSuggestionsFetchRequestedForTitle = ({ value }) => {
    this.setState({
      suggestionsForTitle: getSuggestionsForTitle(value)
    });
  };

  onSuggestionsFetchRequestedForLoc = ({ value }) => {
    this.setState({
      suggestionsForLoc: getSuggestionsForLoc(value)
    });
  };

  onSuggestionsClearRequestedForTitle = () => {
    this.setState({
      suggestionsForTitle: []
    });
  };

  onSuggestionsClearRequestedForLoc = () => {
    this.setState({
      suggestionsForLoc: []
    });
  };

  showDetail = () => {
    // 'searchOrResetButtonMode' is 0 for "Search" and 1 for "Reset"
    if (this.state.searchOrResetButtonMode === 0) {
      document
        .getElementById("autoSuggestDivForTitle")
        .getElementsByTagName("input")[0].disabled = true;
      document
        .getElementById("autoSuggestDivForLoc")
        .getElementsByTagName("input")[0].disabled = true;
      ReactDOM.render(
        <AdditionalSearch
          titleSearchVal={this.state.titleSearchVal}
          locSearchVal={this.state.locSearchVal}
          uniqueJobLocations={uniqueJobLocations}
          jobDetails={mockData}
        />,
        document.getElementById("easCompAdditionalSearch")
      );
    } else {
      document
        .getElementById("autoSuggestDivForTitle")
        .getElementsByTagName("input")[0].disabled = false;
      document
        .getElementById("autoSuggestDivForLoc")
        .getElementsByTagName("input")[0].disabled = false;
      ReactDOM.unmountComponentAtNode(
        document.getElementById("easCompAdditionalSearch")
      );
      ReactDOM.unmountComponentAtNode(
        document.getElementById("easCompJobDetail")
      );
    }
    this.setState({
      searchOrResetButtonMode: this.state.searchOrResetButtonMode === 0 ? 1 : 0
    });
  };

  render() {
    const {
      titleSearchVal,
      suggestionsForTitle,
      locSearchVal,
      suggestionsForLoc
    } = this.state;

    const inputPropsForTitle = {
      placeholder: "Enter Job Title (Partial Match)",
      value: titleSearchVal,
      onChange: this.onChangeForTitle
    };
    const inputPropsForLoc = {
      placeholder: "Enter Job Location (Exact Match)",
      value: locSearchVal,
      onChange: this.onChangeForLoc
    };

    return (
      <div>
        <div id="autoSuggestDivForTitle" className="eas-app-block-mini">
          <Autosuggest
            suggestions={suggestionsForTitle}
            onSuggestionsFetchRequested={
              this.onSuggestionsFetchRequestedForTitle
            }
            onSuggestionsClearRequested={
              this.onSuggestionsClearRequestedForTitle
            }
            getSuggestionValue={getSuggestionValueForTitle}
            renderSuggestion={renderSuggestionForTitle}
            inputProps={inputPropsForTitle}
            id="auto-suggest-title"
          />
        </div>
        <div id="autoSuggestDivForLoc" className="eas-app-block-mini">
          <Autosuggest
            suggestions={suggestionsForLoc}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequestedForLoc}
            onSuggestionsClearRequested={this.onSuggestionsClearRequestedForLoc}
            getSuggestionValue={getSuggestionValueForLoc}
            renderSuggestion={renderSuggestionForLoc}
            inputProps={inputPropsForLoc}
            id="auto-suggest-location"
          />
        </div>
        <div className="eas-app-block-mini">
          <button onClick={() => this.showDetail()}>
            {this.state.searchOrResetButtonMode === 0 ? "Search" : "Reset"}
          </button>
        </div>
      </div>
    );
  }
}

export default AutosuggestSearchApp;
