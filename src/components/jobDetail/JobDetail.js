import React, { Component } from "react";
import ReactDOM from "react-dom";
// import SimpleSearchApp from "../app/SimpleSearchApp";
import AutosuggestSearchApp from '../app/AutosuggestSearchApp';

class JobDetail extends Component {
  constructor(props) {
    super(props);
    this.state = { jobDetail: props.jobDetail };
  }

  goBack = () => {
    ReactDOM.unmountComponentAtNode(document.getElementById('easCompJobDetail'));
    // ReactDOM.render(<SimpleSearchApp />, document.getElementById("easCompSimpleSearchApp"));
    ReactDOM.render(<AutosuggestSearchApp />, document.getElementById("easCompAutosuggestSearchApp"));
  };

  render() {
    return (
      <div>
        <button onClick={() => this.goBack()}>Back</button>
        <table>
          <tbody>
            <tr>
              <td>ID: </td>
              <td>{this.state.jobDetail.id} </td>
            </tr>
            <tr>
              <td>Title: </td>
              <td>{this.state.jobDetail.title} </td>
            </tr>
            <tr>
              <td>Primary skill: </td>
              <td>{this.state.jobDetail.primary_skill} </td>
            </tr>
            <tr>
              <td>Secondary skill: </td>
              <td>{this.state.jobDetail.secondary_skill} </td>
            </tr>
            <tr>
              <td>Domain: </td>
              <td>{this.state.jobDetail.domain} </td>
            </tr>
            <tr>
              <td>Responsibility: </td>
              <td>{this.state.jobDetail.responsibility} </td>
            </tr>
            <tr>
              <td>Experience: </td>
              <td>{this.state.jobDetail.experience} </td>
            </tr>
            <tr>
              <td>Location: </td>
              <td>{this.state.jobDetail.location} </td>
            </tr>
            <tr>
              <td>Job-Code: </td>
              <td>{this.state.jobDetail.jobcode} </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default JobDetail;
